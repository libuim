#include <stdio.h>

#include <uiml_bfish.h>
#include <modp_b64.h>
#include <uiml_dh.h>

int main(int argc, char* argv[]){  
  MCRYPT td;
  char* pwd = 0;
  char* data=0;
  char* b64buf=0;
  char* iv=0;
  size_t len=0;
  
  if(argc==3){
    len=strlen(argv[2]);
    data=malloc(len);
    pwd=argv[1];
  }else{
    printf("usage: %s <key> <string>\n",argv[0]);
    exit(0);
  }
  memset(data,'0',len);
  strncpy(data,argv[2],len);
  
  puts("libuim-test");
  puts("blowfish stuff ...\n");
  
  td=uimc_bf_open();
  if(!td){
    puts("error -> open");
    return 1;
  }
  iv=uimc_bf_init(td,(unsigned char*)pwd,strlen(pwd),0);
  if(!iv){
    //puts("error -> init");
    return 1;
  }
  
  printf("plaintext data = %s\nlen = %i\n",data,(int)len);
  uimc_bf_encrypt(td,data,len);
  b64buf = malloc(modp_b64_encode_len(len));
  if (modp_b64_encode(b64buf, data, len) == -1) {
    printf("Error\n");
    return 1;
  }
  printf("crypted+b64 data = %s\n", b64buf);
  
  if (modp_b64_decode(data, b64buf, strlen(b64buf)) == -1){
    printf("Error\n");
    return 1;
  }
  
  uimc_bf_deinit(td);
  if(!uimc_bf_init(td,(unsigned char*)pwd,strlen(pwd),iv)){
    //puts("error -> init");
    return 1;
  }
  
  uimc_bf_decrypt(td,data,len);
  printf("decrypted data = %s\n",data);
  
  uimc_bf_deinit(td);
  uimc_bf_close(td);
  
  free(iv);
  free(b64buf);
  free(data);
  
  return 0;
}
