/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Copyright (C) unfancy::labs 2008 <sya@unfancy.org>
 *
 * libuim is free software copyrighted by unfancy::labs.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``unfancy::labs'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * libuim IS PROVIDED BY unfancy::labs ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL unfancy::labs OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************/

#ifndef _U_LIBUIM_BFISH_H
#define _U_LIBUIM_BFISH_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <wchar.h>

#include <mcrypt.h>
#include <mhash.h>
 
 /*****************************************************************************/
 
int uimc_bf_keygen(unsigned char* pwd,size_t len, void* key, size_t size);

MCRYPT uimc_bf_open(void);
void uimc_bf_close(MCRYPT td);

char* uimc_bf_init(MCRYPT td,unsigned char* pwd, size_t len,char* iv);
void uimc_bf_deinit(MCRYPT td);

void uimc_bf_encrypt(MCRYPT td,char* data, size_t len);
void uimc_bf_decrypt(MCRYPT td,char* data, size_t len);

#endif //_U_LIBUIM_BFISH_H
