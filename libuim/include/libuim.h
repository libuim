/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Copyright (C) unfancy::labs 2008 <sya@unfancy.org>
 *
 * libuim is free software copyrighted by unfancy::labs.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``unfancy::labs'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * libuim IS PROVIDED BY unfancy::labs ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL unfancy::labs OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************/

#ifndef _U_LIBUIM_H
#define _U_LIBUIM_H

#include <stdio.h>
#include <stdint.h>
#include <wchar.h>
 
/*****************************************************************************/
 
typedef struct peer_ peer;
 struct peer_ {
	 uint32_t id;
	 wchar_t* name;
	 peer *next_peer;
	 void* dummy1;
	 void* dummy2;
	 void* dummy3;
 };
 
/*****************************************************************************/
 
void uiml_init(void);
void uiml_exit(void);

uint32_t uiml_event_pending(void);
void uiml_event_handle(void);

void uiml_config_set(wchar_t* setting, wchar_t* value);
wchar_t* uiml_config_get(wchar_t* setting);

void uiml_peer_add(wchar_t* name);
void uiml_peer_remove(peer*);
uint32_t uiml_peer_get_count(void);
peer* uiml_peer_get_byid(uint32_t);
peer* uiml_peer_get_byname(wchar_t* name);

//void uiml_send_raw(struct pkg* mypkg);
void uiml_send_msg(peer* to, wchar_t* msg);
void uiml_send_req_keyex(peer* to);

#endif /* _U_LIBUIM_H */
