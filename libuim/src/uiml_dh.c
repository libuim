/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Copyright (C) unfancy::labs 2008 <sya@unfancy.org>
 *
 * libuim is free software copyrighted by unfancy::labs.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``unfancy::labs'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * libuim IS PROVIDED BY unfancy::labs ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL unfancy::labs OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*****************************************************************************/

#include "uiml_dh.h"
#include "uiml_random.h"

/*****************************************************************************/

struct uimc_dh_ctx* uimc_dh_new(uint16_t bits){
    struct uimc_dh_ctx* ctx=0;
    char* key=0;
    
    ctx=malloc(sizeof(struct uimc_dh_ctx));
    if(!ctx){
        puts("dh ctx alloc!");
        exit(EXIT_FAILURE);
    }
    memset(ctx,'0',sizeof(struct uimc_dh_ctx));
    key=malloc(sizeof(char)*(bits/8));
    if(!ctx){
        puts("dh key alloc!");
        exit(EXIT_FAILURE);
    }

    //init stuff ...
    mpz_init(ctx->g);
    mpz_init(ctx->p);
    mpz_init(ctx->prk);
    mpz_init(ctx->puk);
    mpz_init(ctx->key);

    //generate random key
    uimc_rnd_getbytes(key,bits/8);

    mpz_set_str(ctx->prk,key,10);
    mpz_set_str(ctx->g,"2",10); // 5 works too

    switch(bits){
    case 1080:
        mpz_set_str(ctx->p,UIMC_DH_GRP_1080,10);
        break;
    case 3072:
    default:
        mpz_set_str(ctx->p,UIMC_DH_GRP_3072,16);
        break;
    case 4096:
        mpz_set_str(ctx->p,UIMC_DH_GRP_4096,16);
        break;
    case 8192:
        mpz_set_str(ctx->p,UIMC_DH_GRP_8192,16);
        break;
    }

    return ctx;
}

void uimc_dh_del(struct uimc_dh_ctx* ctx){
    mpz_clear(ctx->g);
    mpz_clear(ctx->p);
    mpz_clear(ctx->prk);
    mpz_clear(ctx->puk);
    mpz_clear(ctx->key);

    free(ctx);
    ctx=0;
}

void uimc_dh_compute(struct uimc_dh_ctx* ctx){
    if(!ctx->puk){
        mpz_powm(ctx->puk,ctx->g,ctx->prk,ctx->p);
    }else{
        mpz_powm(ctx->key,ctx->puk,ctx->prk,ctx->p);
    }
}

/*class dh_ke(object):
    def __init__(self,verbose=False):
        self.v=verbose
        self.a=gmpy.mpz(random.getrandbits(dh_ke.bits))
        if self.v:print "g:"+str(dh_ke.g)
        if self.v:print "p:"+str(dh_ke.p)
        if self.v:print "priv:"+str(self.a)
        if self.v:print "\n"
    def phase1(self):
#        self.A=dh_ke.g**self.a%dh_ke.p
        self.A=pow(dh_ke.g,self.a,dh_ke.p)
        return self.A
    def phase2(self,B):
#        self.K=gmpy.mpz(B)**self.a%dh_ke.p
        self.K=pow(gmpy.mpz(B),self.a,dh_ke.p)
        return self.K
*/	
