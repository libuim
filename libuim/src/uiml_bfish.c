/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Copyright (C) unfancy::labs 2008 <sya@unfancy.org>
 *
 * libuim is free software copyrighted by unfancy::labs.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``unfancy::labs'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * libuim IS PROVIDED BY unfancy::labs ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL unfancy::labs OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*****************************************************************************/

//#include "libuim.h"
#include "uiml_bfish.h"

/*****************************************************************************/

int uimc_bf_keygen(unsigned char* pwd,size_t len, void* key, size_t size){
	KEYGEN data;
	data.hash_algorithm[0] = MHASH_SHA1;
	data.count = 0;
	data.salt = 0;
	data.salt_size = 0;
	return mhash_keygen_ext(KEYGEN_MCRYPT, data,key,size, pwd,len);
}

MCRYPT uimc_bf_open(void){
        MCRYPT td;
	td = mcrypt_module_open("blowfish", NULL, "cfb", NULL);
	if (td==MCRYPT_FAILED) {
		return (MCRYPT)0;
	}
	return td;
}

void uimc_bf_close(MCRYPT td){
	/* Unload the loaded module */
	mcrypt_module_close(td);
}

char* uimc_bf_init(MCRYPT td,unsigned char* pwd, size_t len,char* iv){
	int keysize=56; /* 56bytes ... 448bits */
	char *key;
	int i;
	key=malloc(keysize);
	uimc_bf_keygen(pwd,len,key,keysize);
    if(!iv){
        iv = malloc(mcrypt_enc_get_iv_size(td));
        /* Put random data in IV. consider using /dev/random or /dev/urandom. */
        srand(time(0));
        for (i=0; i< mcrypt_enc_get_iv_size( td); i++) {
            iv[i]=rand();
        }
    }
	i=mcrypt_generic_init(td, key, keysize, iv);
	if (i<0) {
		mcrypt_perror(i);
		return 0;
	}
    free(key);
	return iv;
}

void uimc_bf_deinit(MCRYPT td){
	/* deinitialize the encryption thread */
	mcrypt_generic_deinit (td);
}

void uimc_bf_encrypt(MCRYPT td,char* data, size_t len){
	int i;
	
	/*char * dat;
	dat=malloc(8+len+1);
	memset(dat,'\0',8);
	memcpy(dat+8,data,len+1);*/
	
	/*char *tmp;
	tmp=malloc(8);
	memset(tmp,'a',8);*/
	
	/* Encryption in CFB is performed in bytes */
	/*for (i=0;i<8;i++){
		mcrypt_generic (td, &tmp[i],1);
        }*/
	for (i=0;i<len;i++){
		mcrypt_generic (td, &data[i],1);
	}
	
	/*
	char block_buffer;
	for (i=0;i<len;i++){
		block_buffer = data[i];
		printf("%c",block_buffer);
		mcrypt_generic (td, &block_buffer, 1);
		data[i]=block_buffer;
	}*/
	
	/*while ( fread (&block_buffer, 1, 1, stdin) == 1 ) {
		mcrypt_generic (td, &block_buffer, 1);
		// Comment above and uncomment this to decrypt 
		//    mdecrypt_generic (td, &block_buffer, 1);  
		fwrite ( &block_buffer, 1, 1, stdout);
	}*/
}
 
void uimc_bf_decrypt(MCRYPT td,char* data, size_t len){
	int i;
	/*char *tmp;
	tmp=malloc(8);
	memset(tmp,'a',8);*/
	
	/* Decryption in CFB is performed in bytes */
	/*for (i=0;i<8;i++){
		mdecrypt_generic (td, &tmp[i],1);
        }*/
	for (i=0;i<len;i++){
		mdecrypt_generic (td, &data[i],1);
	}
}
